var     express=require('express'),
        app= express();

app.set("view engine","ejs");

var campgrounds=[
        {name : "mountain geek", image: "http://www.trbimg.com/img-5949b1f7/turbine/la-1498001907-9zu1omzurz-snap-image"},
        {name : "stone mountain", image: "https://www.passport-america.com/campgrounds/images/2386_1.png"},
        {name : "bukamba range", image: "https://static.rootsrated.com/image/upload/s--_i1nqUT1--/t_rr_large_traditional/oy9xsbijv8wehnrzv0zt.jpg"},
        {name : "wind laster", image: "https://20dqe434dcuq54vwy1wai79h-wpengine.netdna-ssl.com/wp-content/uploads/2016/09/Moraine-Park-Campground-Tinkurlab-OutThere-Colorado.jpg"},
        {name : "pine vally", image: "https://www.nps.gov/buff/planyourvisit/images/TylerBend.jpg"}
        ]
var quotes=[
        {name:"woolimer",description:"Mountain geek is like the godess of campgrounds",bgcolor:"bg-success",textcolor:"text-light"},
        {name:"woolimer",description:"Mountain geek is like the godess of campgrounds",bgcolor:"bg-dark",textcolor:"text-light"},
        {name:"woolimer",descric9ption:"Mountain geek is like the godess of campgrounds",bgcolor:"bg-primary",textcolor:"text-light"},
        {name:"woolimer",description:"Mountain geek is like the godess of campgrounds",bgcolor:"bg-light",textcolor:"text-danger"}
    ]

app.get("/",function(req,res){
    res.render("main",{campgrounds:campgrounds,quotes:quotes});
});

app.get("/campgrounds",function(req,res){

    res.render("campgrounds",{campgrounds:campgrounds});
});

app.get("/quotes",function(req,res){
    res.render("quotes",{quotes:quotes})
});

app.get("/login",function(req,res){
    res.render("login")
});

app.listen(process.env.PORT,process.env.IP,function(){
    console.log("server has started");
})